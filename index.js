import express from 'express'
import path from 'path'
import { parseData } from './middleware.js'
import { succeedResponse } from './response.js'
import mongoose from 'mongoose';

mongoose.connect("mongodb+srv://admin:12345@cluster0.8pyiz.mongodb.net/test", { useNewUrlParser: true , useUnifiedTopology: true })

//create a data schema

const clientSchema = {
    datetime: String,
    client: String,
    type: String,
    description: String,
    status: String,
    createdDt: {
        type: Date,
        default: Date.now
    }
}

const Client = mongoose.model("Clients", clientSchema)



const __dirname = path.resolve()
const PORT = process.env.PORT ?? 3000

const app = express()

app.use(express.static(path.resolve(__dirname, 'static')))
app.use(express.static(__dirname + '/public'));


//Setting view engine
app.set('view engine', 'ejs')
app.set('views', path.resolve(__dirname, 'ejs'))

app.get('/', (req, res) => {
   res.render('index')
})





// Creating Post Route for Submit Form

app.post('/form', parseData, async (req, res) => {
    // Retrive form data from request object
    const data = req.body
    let newClient = new Client(data)
    await newClient.save();
    const clients = await Client.find()
        .sort({createdDt: 'desc'})
        .limit(5);
    res.send(succeedResponse(
        'ok',
        clients.map(item => {
            return {
                 datetime: item.datetime,
                 client: item.client,
                 type: item.type,
                 description: item.description
            }
        })
    ));
})

 //Get client api
app.get('/get-clients', async (req, res) => {
    const clients = await Client.find()
        .sort({createdDt: 'desc'})
        .limit(5);
    res.send(succeedResponse(
        'ok',
        clients.map(item => {
               return {
                    datetime: item.datetime,
                    client: item.client,
                    type: item.type,
                    description: item.description
               }
        })
    ));
 });



app.listen(3000, () => {
    console.log(`Server has been started on port ${PORT}....`)
    
})